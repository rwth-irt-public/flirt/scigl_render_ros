/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <scigl_render_ros/projector_calib.hpp>
#include <opencv2/core/persistence.hpp>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

namespace scigl_render_ros
{
ProjectorCalib::ProjectorCalib()
{
}

bool ProjectorCalib::load(const std::string& filename)
{
  cv::FileStorage calib_file;
  if (calib_file.open(filename, cv::FileStorage::READ, "utf-8"))
  {
    calib_file["image_width"] >> image_width;
    calib_file["image_height"] >> image_height;
    calib_file["camera_name"] >> camera_name;
    calib_file["reprojection_error"] >> reprojection_error;
    calib_file["camera_matrix"] >> camera_matrix;
    calib_file["distortion_model"] >> distortion_model;
    calib_file["distortion_coefficients"] >> distortion_coefficients;
    calib_file["projection_matrix"] >> projection_matrix;
    calib_file["rotation_matrix"] >> rotation_matrix;
    calib_file["quaternion"] >> quaternion;
    calib_file["translation_vector"] >> translation_vector;
    calib_file["tf_matrix"] >> tf_matrix;
    calib_file.release();
    return true;
  }
  else
  {
    return false;
  }
}

void ProjectorCalib::save(const std::string& filename) const
{
  cv::FileStorage calib_file(filename, cv::FileStorage::WRITE, "utf-8");
  calib_file << "image_width" << image_width;
  calib_file << "image_height" << image_height;
  calib_file << "camera_name" << camera_name;
  calib_file << "reprojection_error" << reprojection_error;
  calib_file << "camera_matrix" << camera_matrix;
  calib_file << "distortion_model" << distortion_model;
  calib_file << "distortion_coefficients" << distortion_coefficients;
  calib_file << "projection_matrix" << projection_matrix;
  calib_file << "rotation_matrix" << rotation_matrix;
  calib_file << "quaternion" << quaternion;
  calib_file << "translation_vector" << translation_vector;
  calib_file << "tf_matrix" << tf_matrix;
  calib_file.release();
}

scigl_render::CvCamera ProjectorCalib::create_cv_camera() const
{
  scigl_render::CameraIntrinsics cam_intrinsics = {};
  cam_intrinsics.width = image_width;
  cam_intrinsics.height = image_height;
  cam_intrinsics.f_x = camera_matrix.at<double>(0, 0);
  cam_intrinsics.f_y = camera_matrix.at<double>(1, 1);
  cam_intrinsics.c_x = camera_matrix.at<double>(0, 2);
  cam_intrinsics.c_y = camera_matrix.at<double>(1, 2);
  cam_intrinsics.s = 0;
  cam_intrinsics.near = 0.1;
  cam_intrinsics.far = 10;
  // limit dist coefficients to rational polynomial
  if (distortion_coefficients.size().area() > 8)
  {
    std::copy_n(distortion_coefficients.begin<double>(), 8, std::begin(cam_intrinsics.dist_coeffs));
  }
  else
  {
    std::copy(distortion_coefficients.begin<double>(), distortion_coefficients.end<double>(),
              std::begin(cam_intrinsics.dist_coeffs));
  }
  return scigl_render::CvCamera(cam_intrinsics);
}

geometry_msgs::TransformStamped ProjectorCalib::get_extrinsic_tf(const std::string& camera_frame,
                                                                 const std::string& projector_frame) const
{
  geometry_msgs::TransformStamped tf_stamped;
  tf_stamped.header.stamp = ros::Time::now();
  tf_stamped.header.frame_id = camera_frame;
  tf_stamped.child_frame_id = projector_frame;
  // Calibration from OpenCV stereoCalibrate
  // Object Pose in frame_1: (R_1, T_1) frame_2: (R_2, T_2)
  // R and T are defined: R_2 = R * R_1  and T_2 = R * T_1 + T (passive)
  // ROS: frame_id -> child_frame_id (active transformation)
  // This means the inverse of the OpenCV transformation is required
  tf2::Transform transform;
  transform.setRotation(tf2::Quaternion(quaternion.at<double>(0, 0), quaternion.at<double>(1, 0),
                                        quaternion.at<double>(2, 0), quaternion.at<double>(3, 0)));
  transform.setOrigin(tf2::Vector3(translation_vector.at<double>(0, 0), translation_vector.at<double>(1, 0),
                                   translation_vector.at<double>(2, 0)));
  tf_stamped.transform = tf2::toMsg(transform.inverse());
  return tf_stamped;
}

}  // namespace scigl_render_ros