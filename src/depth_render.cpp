/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <cstring>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <ros/console.h>
#include <scigl_render/check_gl_error.hpp>
#include <scigl_render_ros/depth_render.hpp>
#include <scigl_render_ros/scigl_convert.hpp>

namespace scigl_render_ros
{
// matching format in OpenCV
const std::string IMAGE_ENCODING = sensor_msgs::image_encodings::TYPE_32FC1;

DepthRender::DepthRender(const std::string& mesh_path, const scigl_render::CameraIntrinsics& intrinsics)
  : gl_context(false, false, intrinsics.width, intrinsics.height)
  , depth_simulator(scigl_render::CvCamera(intrinsics), scigl_render::Model(mesh_path))
{
  auto texture = std::make_shared<scigl_render::Texture2D>(
      intrinsics.width, intrinsics.height, scigl_render::DepthSimulator::FORMAT,
      scigl_render::DepthSimulator::INTERNAL_FORMAT, scigl_render::DepthSimulator::TYPE);
  framebuffer = std::make_shared<scigl_render::FrameBuffer>(texture);
  fbo_reader = std::unique_ptr<scigl_render::FramebufferReader>(
      new scigl_render::FramebufferReader(framebuffer, scigl_render::DepthSimulator::PIXEL_SIZE));
  // cv::Mat::create will allocate continous memory
  image_buffer.create(intrinsics.height, intrinsics.width);
  // Configure the global rendering settings
  glColorMask(GL_TRUE, GL_FALSE, GL_FALSE, GL_FALSE);
  glEnable(GL_DEPTH_TEST);
  scigl_render::check_gl_error("creating depth render");
}

auto DepthRender::render(const geometry_msgs::TransformStamped& camera_pose,
                         const geometry_msgs::TransformStamped& object_pose) -> sensor_msgs::ImagePtr
{
  try
  {
    // render the object on top
    framebuffer->clear();
    framebuffer->activate();
    depth_simulator.render_pose(SciglConvert::convert_tf(object_pose), SciglConvert::convert_tf(camera_pose));
    framebuffer->deactivate();
    // start new asynchronous read operation
    fbo_reader->start_read();
    // read the last frame synchronously
    void* data = fbo_reader->do_read();
    memcpy(image_buffer.data, data, image_buffer.total() * image_buffer.elemSize());
    fbo_reader->end_read();
    fbo_reader->swap_buffers();
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
  }
  // create ros message
  auto res = SciglConvert::convert_gl_image(image_buffer, IMAGE_ENCODING);
  return res;
}
}  // namespace scigl_render_ros