/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <cstring>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <ros/console.h>
#include <scigl_render/check_gl_error.hpp>
#include <scigl_render/shader/shader_builder.hpp>
#include <scigl_render_ros/ar_render.hpp>
#include <scigl_render_ros/scigl_convert.hpp>

const std::string vs_source =
#include <scigl_render/shader/distort.vert>
    "";
const std::string fs_source =
#include <scigl_render/shader/texture.frag>
    "";

namespace scigl_render_ros
{
// image format in OpenGL
const GLint INTERNAL_FORMAT = GL_RGB8;
const GLenum FORMAT = GL_BGR;
const GLenum TYPE = GL_UNSIGNED_BYTE;
// matching format in OpenCV
const std::string IMAGE_ENCODING = sensor_msgs::image_encodings::BGR8;
const int CV_TYPE = CV_8UC3;

ArRender::ArRender(const std::string& mesh_path, const sensor_msgs::CameraInfoConstPtr& camera_info, double min_depth,
                   double max_depth)
  : gl_context(false, false, camera_info->width, camera_info->height)
  , camera(SciglConvert::convert_camera_info(camera_info, min_depth, max_depth))
  , model(mesh_path)
{
  // framebuffer for offscreen rendering
  texture =
      std::make_shared<scigl_render::Texture2D>(camera_info->width, camera_info->height, FORMAT, INTERNAL_FORMAT, TYPE);
  framebuffer = std::make_shared<scigl_render::FrameBuffer>(texture);
  fbo_reader = std::unique_ptr<scigl_render::FramebufferReader>(
      new scigl_render::FramebufferReader(framebuffer, 3 * sizeof(uint8_t)));
  // build the shader
  scigl_render::ShaderBuilder shader_builder;
  shader_builder.attach_vertex_shader(vs_source);
  shader_builder.attach_fragment_shader(fs_source);
  shader = shader_builder.build();
  // cv::Mat::create allocates continuos memory
  image_buffer.create(camera_info->height, camera_info->width, CV_TYPE);
  // Configure the global rendering settings
  light.color = glm::vec3(1, 1, 1);
  glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
  scigl_render::check_gl_error("creating ArRender");
}

auto ArRender::render(const geometry_msgs::TransformStamped& camera_pose,
                      const geometry_msgs::TransformStamped& object_pose,
                      const geometry_msgs::TransformStamped& light_pose, const sensor_msgs::ImageConstPtr& image)
    -> sensor_msgs::ImagePtr
{
  try
  {
    // convert the image format (bottom left origin)
    auto gl_image = SciglConvert::convert_ros_image(image, IMAGE_ENCODING);
    // convert the poses
    camera.pose = SciglConvert::convert_tf(camera_pose);
    light.position = SciglConvert::convert_tf(light_pose).position;
    model.pose = SciglConvert::convert_tf(object_pose);
    // clear OpenGL swap_buffers
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    // render the image
    framebuffer->clear();
    framebuffer->activate();
    texture->store_image(gl_image.data);
    image_render.draw(texture);
    // render the object on top
    camera.set_in_shader(shader);
    light.set_in_shader(shader);
    shader.activate();
    model.draw(shader);
    shader.deactivate();
    framebuffer->deactivate();
    scigl_render::check_gl_error("rendered next frame");
    // start asynchronous read operation
    fbo_reader->start_read();
    // read the last frame synchronously
    void* data = fbo_reader->do_read();
    memcpy(image_buffer.data, data, image_buffer.total() * image_buffer.elemSize());
    fbo_reader->end_read();
    fbo_reader->swap_buffers();
  }
  catch (cv_bridge::Exception& e)
  {
    ROS_ERROR("cv_bridge exception: %s", e.what());
  }
  // create ros message
  auto res = SciglConvert::convert_gl_image(image_buffer, IMAGE_ENCODING);
  res->header.frame_id = image->header.frame_id;
  res->header.stamp = image->header.stamp;
  return res;
}
}  // namespace scigl_render_ros