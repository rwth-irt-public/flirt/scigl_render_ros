/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <functional>
#include <scigl_render_ros/projector_node.hpp>

namespace ph = std::placeholders;

namespace scigl_render_ros
{
ProjectorNode::ProjectorNode() : tf2_listener(tf2_buffer)
{
  ros::NodeHandle private_nh("~");
  private_nh.param<std::string>("world_frame_id", world_frame_id, "world");
  private_nh.param<std::string>("camera_frame_id", camera_frame_id, "camera_color_optical_frame");
  private_nh.param<std::string>("object_frame_id", object_frame_id, "tracked_object");
  private_nh.param<std::string>("light_frame_id", light_frame_id, "light");
  private_nh.param<std::string>("projector_frame_id", projector_frame_id, "projector");
  std::string mesh_path;
  private_nh.param<std::string>("mesh_path", mesh_path, "mesh.dae");
  // setup rendering
  try
  {
    ROS_INFO_STREAM("loading model " << mesh_path);
    projector.set_model(mesh_path);
  }
  catch (const std::runtime_error& error)
  {
    fatal_error("Failed to load " + mesh_path + " message: " + error.what());
  }
  std::string calibration_path;
  private_nh.param<std::string>("calibration_path", calibration_path, "projector.yml");
  ROS_INFO("loading projector calibration");
  if (!set_projector_calibration(calibration_path))
  {
    fatal_error("Failed to load " + calibration_path);
  }
  // dynamic reconfiguration server
  ROS_INFO("Reconfiguration server setup");
  reconfig_server.setCallback(std::bind(&ProjectorNode::reconfig_callback, this, ph::_1));
  // create timer for rendering at the given framerate
  ROS_INFO("Render timer setup");
  int frame_rate;
  private_nh.param<int>("frame_rate", frame_rate, 30);
  render_timer = node_handle.createTimer(ros::Duration(1.0 / frame_rate),
                                         std::bind(&ProjectorNode::render_timer_callback, this, ph::_1), false, false);
}

void ProjectorNode::fatal_error(const std::string& msg)
{
  ROS_FATAL_STREAM(msg);
  ros::shutdown();
  exit(EXIT_FAILURE);
}

void ProjectorNode::reconfig_callback(scigl_render_ros::projectorConfig& confg)
{
  projector.set_fullscreen(confg.fullscreen);
}

void ProjectorNode::render_timer_callback(const ros::TimerEvent&)
{
  try
  {
    if (projector.escape_pressed())
    {
      ros::shutdown();
      return;
    }
    if (tf2_buffer.canTransform(world_frame_id, projector_frame_id, ros::Time(0)) &&
        tf2_buffer.canTransform(world_frame_id, object_frame_id, ros::Time(0)) &&
        tf2_buffer.canTransform(world_frame_id, light_frame_id, ros::Time(0)))
    {
      auto projector_pose = tf2_buffer.lookupTransform(world_frame_id, projector_frame_id, ros::Time(0));
      auto object_pose = tf2_buffer.lookupTransform(world_frame_id, object_frame_id, ros::Time(0));
      auto light_pose = tf2_buffer.lookupTransform(world_frame_id, light_frame_id, ros::Time(0));
      projector.render(projector_pose, object_pose, light_pose);
    }
  }
  catch (tf2::TransformException& ex)
  {
    // don't crash the node if the tf is faulty
    ROS_WARN("%s", ex.what());
    ros::Duration(1.0).sleep();
  }
}

void ProjectorNode::run()
{
  render_timer.start();
  ROS_INFO("Projector is running");
  ros::spin();
}

bool ProjectorNode::set_projector_calibration(const std::string& path)
{
  ProjectorCalib proj_calib;
  if (proj_calib.load(path))
  {
    // publish extrinsics
    camera_to_projector_broadcaster.sendTransform(proj_calib.get_extrinsic_tf(camera_frame_id, projector_frame_id));
    // set intrinsics
    projector.set_calibration(proj_calib);
    return true;
  }
  else
  {
    return false;
  }
}

}  // namespace scigl_render_ros

int main(int argc, char** argv)
{
  ros::init(argc, argv, "projector_node");
  ros::NodeHandle nh;
  scigl_render_ros::ProjectorNode projector_node;
  // will block/spin
  projector_node.run();
  ROS_INFO("projector node comleted");
  return EXIT_SUCCESS;
}
