/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <geometry_msgs/TransformStamped.h>
#include <ros/ros.h>
#include <tf2/LinearMath/Transform.h>
#include <tf2_ros/transform_broadcaster.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

int main(int argc, char** argv)
{
  // init node
  ros::init(argc, argv, "dummy_motion_node");
  ros::NodeHandle node_handle;
  ros::NodeHandle pnh("~");
  // params
  double frame_rate = 0;
  pnh.param<double>("frame_rate", frame_rate, 30);
  ros::Rate rate(frame_rate);
  // Velocities in m/s and rad/s
  double v_x = 0;
  pnh.param<double>("v_x", v_x, 5e-3);
  double v_y = 0;
  pnh.param<double>("v_y", v_y, 0);
  double v_z = 0;
  pnh.param<double>("v_z", v_z, 1e-2);
  double rv_x = 0;
  pnh.param<double>("rv_x", rv_x, 1e-3);
  double rv_y = 0;
  pnh.param<double>("rv_y", rv_y, 1e-3);
  double rv_z = 0;
  pnh.param<double>("rv_z", rv_z, 1e-3);
  // create tf scaled by frame rate
  tf2::Vector3 velocity(v_x / frame_rate, v_y / frame_rate, v_z / frame_rate);
  tf2::Quaternion rotation;
  // As always Tait Bryan ZYX
  rotation.setRPY(rv_x / frame_rate, rv_y / frame_rate, rv_z / frame_rate);
  rotation.normalize();
  // parameters for movement
  double min_z = 0;
  pnh.param<double>("min_z", min_z, 0);
  double max_z = 0;
  pnh.param<double>("max_z", max_z, 0.2);
  // pose to publish
  geometry_msgs::TransformStamped pose;
  pnh.param<std::string>("parent_frame_id", pose.header.frame_id, "world");
  pnh.param<std::string>("object_frame_id", pose.child_frame_id, "dummy_object");
  // initial pose
  double p_x = 0;
  pnh.param<double>("p_x", p_x, 0);
  double p_y = 0;
  pnh.param<double>("p_y", p_y, 0);
  double p_z = 0;
  pnh.param<double>("p_z", p_z, min_z);
  double r_x = 0;
  pnh.param<double>("r_x", r_x, 0);
  double r_y = 0;
  pnh.param<double>("r_y", r_y, 0);
  double r_z = 0;
  pnh.param<double>("r_z", r_z, 0);
  // As always Tait Bryan ZYX
  tf2::Quaternion init_ori;
  init_ori.setRPY(r_x, r_y, r_z);
  tf2::Transform transform(init_ori, tf2::Vector3(p_x, p_y, p_z));
  pose.transform = tf2::toMsg(transform);
  // tf2_ros
  tf2_ros::TransformBroadcaster tf_broadcast;
  // looping
  // start a bit later so the other node can get ready
  for (int i = 0; i < 60; i++)
  {
    pose.header.stamp = ros::Time::now();
    tf_broadcast.sendTransform(pose);
    rate.sleep();
  }
  while (ros::ok())
  {
    transform.getOrigin() += velocity;
    transform.setRotation(rotation * transform.getRotation());
    pose.transform = tf2::toMsg(transform);
    pose.header.stamp = ros::Time::now();
    tf_broadcast.sendTransform(pose);
    // turn around?
    if (pose.transform.translation.z > max_z || pose.transform.translation.z < min_z)
    {
      velocity = -1 * velocity;
      rotation = rotation.inverse();
    }
    rate.sleep();
  }
  return EXIT_SUCCESS;
}  // end main()