/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#include <scigl_render/shader/shader_builder.hpp>
#include <scigl_render_ros/projector.hpp>
#include <scigl_render_ros/projector_calib.hpp>
#include <scigl_render_ros/scigl_convert.hpp>

namespace scigl_render_ros
{
Projector::Projector() : gl_context(true, false, 1920, 1080)
{
  // shader with distortion capabilities
  const std::string vs_source =
#include <scigl_render/shader/distort.vert>
      "";
  const std::string fs_source =
#include <scigl_render/shader/texture.frag>
      "";
  scigl_render::ShaderBuilder shader_builder;
  shader_builder.attach_vertex_shader(vs_source);
  shader_builder.attach_fragment_shader(fs_source);
  onscreen_render.set_shader_program(shader_builder.build());
  // white light
  light.color = glm::vec3(1, 1, 1);
}

bool Projector::escape_pressed()
{
  // process inputs
  glfwPollEvents();
  if (glfwGetKey(gl_context.get_window(), GLFW_KEY_ESCAPE) == GLFW_PRESS)
  {
    return true;
  }
  return false;
}

void Projector::render(const geometry_msgs::TransformStamped& projector_pose,
                       const geometry_msgs::TransformStamped& object_pose,
                       const geometry_msgs::TransformStamped& light_pose)
{
  if (!model)
  {
    return;
  }
  projector.pose = SciglConvert::convert_tf(projector_pose);
  light.position = SciglConvert::convert_tf(light_pose).position;
  model->pose = SciglConvert::convert_tf(object_pose);
  onscreen_render.next_frame(gl_context, projector, *model, light);
}

void Projector::set_fullscreen(bool enabled)
{
  gl_context.set_fullscreen(enabled);
}

void Projector::set_model(const std::string& model_path)
{
  using scigl_render::Model;
  model = std::unique_ptr<Model>(new Model(model_path));
}

void Projector::set_calibration(const ProjectorCalib& calibration)
{
  projector = calibration.create_cv_camera();
  gl_context.set_resolution(projector.get_intrinsics().width, projector.get_intrinsics().height);
}

}  // namespace scigl_render_ros