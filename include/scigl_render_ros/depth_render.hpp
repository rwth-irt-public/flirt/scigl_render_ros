/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <GL/gl3w.h>
#include <opencv2/core/core.hpp>
#include <geometry_msgs/TransformStamped.h>
#include <scigl_render/gl_context.hpp>
#include <scigl_render/buffer/frame_buffer.hpp>
#include <scigl_render/buffer/frame_buffer_reader.hpp>
#include <scigl_render/render/depth_simulator.hpp>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>

namespace scigl_render_ros
{
/*!
Simulating depth values using the scigl_render library
*/
class DepthRender
{
public:
  /*!
  Configures the render class.
  \param mesh_path the 3D model to render
  \param camera_info contains the intrinsic parameters
  \param min_depth the shortest range to measure by the camera
  \param max_depth the longest range mo measure by the camera
  */
  DepthRender(const std::string& mesh_path, const scigl_render::CameraIntrinsics& intrinsics);

  /*!
  Renders the configured model as depth image using the cameras intrinsics.
  \param camera_pose the pose of the camera n the world frame (ROS convention:
  http://docs.ros.org/api/sensor_msgs/html/msg/CameraInfo.html)
  \param object_pose the pose of the object in the world frame
  \return an image with the object render into
  */
  sensor_msgs::ImagePtr render(const geometry_msgs::TransformStamped& camera_pose,
                               const geometry_msgs::TransformStamped& object_pose);

private:
  // draw offscreen
  scigl_render::GLContext gl_context;
  std::shared_ptr<scigl_render::FrameBuffer> framebuffer;
  std::unique_ptr<scigl_render::FramebufferReader> fbo_reader;
  cv::Mat_<float> image_buffer;
  // rendering scene
  scigl_render::DepthSimulator depth_simulator;
};
}  // namespace scigl_render_ros