/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <opencv2/core/core.hpp>
#include <scigl_render/scene/cv_camera.hpp>
#include <geometry_msgs/TransformStamped.h>
#include <string>

namespace scigl_render_ros
{
/*!
Loads and saves the projectors calibration data.
*/
class ProjectorCalib
{
public:
  /*!
  Default constructor creates the object without any initialization.
  */
  ProjectorCalib();

  /*!
  Load the calibration from the given calibration .yml file.
  \param filenamee filename the path to the calibration .yml file
  \returns true on success, false otherwise
  */
  bool load(const std::string& filename);

  /*!
  Save the calibration to the given calibration .yml file.
  \param filenamee filename the path to the calibration .yml file
  */
  void save(const std::string& filename) const;

  /*!
  Create a scigl_render CvCamera from this projector calibration.
  */
  scigl_render::CvCamera create_cv_camera() const;

  /*!
  Calculates the pose of the projector in the camera frame.
  \param camera_frame the cameras frame name
  \param projector_frame the projectors frame name
  */
  geometry_msgs::TransformStamped get_extrinsic_tf(const std::string& camera_frame = "camera",
                                                   const std::string& projector_frame = "projector") const;

private:
  // members equal the names of the parameters in the .yml file
  int image_width;
  int image_height;
  std::string camera_name;
  double reprojection_error;
  cv::Mat camera_matrix;
  std::string distortion_model;
  cv::Mat distortion_coefficients;
  cv::Mat projection_matrix;
  cv::Mat rotation_matrix;
  cv::Mat quaternion;
  cv::Mat translation_vector;
  cv::Mat tf_matrix;
};
}  // namespace scigl_render_ros