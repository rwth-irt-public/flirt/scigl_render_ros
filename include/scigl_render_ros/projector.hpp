/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <geometry_msgs/TransformStamped.h>
#include <scigl_render/gl_context.hpp>
#include <scigl_render/render/onscreen_render.hpp>
#include <scigl_render/scene/cv_camera.hpp>
#include <scigl_render/scene/diffuse_light.hpp>
#include <scigl_render/scene/model.hpp>
#include <scigl_render/shader/shader.hpp>
#include <scigl_render_ros/projector_calib.hpp>

namespace scigl_render_ros
{
/*!
Augmented reality rendering of 3D models via a calibrated projector. The
calibration is expected to be in the fashion of the 3d_projector_calibration
project.
*/
class Projector
{
public:
  /*!
  Initializes the OpenGL rendering environment.
  Don't forget to set the model and calibration data.
  */
  Projector();

  /*!
  Command to exit the projection
  */
  bool escape_pressed();

  /*!
  Renders the current scene to the glfw windows.
  \param projector_pose the current pose of the projector in world coordinates
  \param object_pose the current pose of the model in world coordinates
  \param light_pose the current pose of the light in world coordinates
  */
  void render(const geometry_msgs::TransformStamped& projector_pose, const geometry_msgs::TransformStamped& object_pose,
              const geometry_msgs::TransformStamped& light_pose);

  /*!
  Enable or disable fullscreen.
  */
  void set_fullscreen(bool enabled);

  /*!
  Set the model to project.
  Will throw runtime_error on fail.
  \param model_path will be rendered
  */
  void set_model(const std::string& model_path);

  /*!
  Set the intrinsic parameters of the projector
  \param calibration contains the projectors intrinsics
  */
  void set_calibration(const ProjectorCalib& calibration);

private:
  // rendering context
  scigl_render::GLContext gl_context;
  scigl_render::OnscreenRender onscreen_render;
  // scene
  scigl_render::CvCamera projector;
  scigl_render::DiffuseLight light;
  std::unique_ptr<scigl_render::Model> model;
};
}  // namespace scigl_render_ros