/* Copyright (c) 2020, Institute of Automatic Control - RWTH Aachen University
   All rights reserved. */

#pragma once
#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>
#include <ros/timer.h>
#include <scigl_render_ros/projector.hpp>
#include <scigl_render_ros/projectorConfig.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf2_ros/transform_listener.h>

namespace scigl_render_ros
{
class ProjectorNode
{
public:
  /*!
  Create the node and load parameters.
  */
  ProjectorNode();

  void run();

private:
  // ros
  ros::NodeHandle node_handle;
  dynamic_reconfigure::Server<scigl_render_ros::projectorConfig> reconfig_server;
  tf2_ros::Buffer tf2_buffer;
  tf2_ros::TransformListener tf2_listener;
  tf2_ros::StaticTransformBroadcaster camera_to_projector_broadcaster;
  // parameters
  std::string world_frame_id;
  std::string camera_frame_id;
  std::string object_frame_id;
  std::string light_frame_id;
  std::string projector_frame_id;
  // projector rendering
  ros::Timer render_timer;
  Projector projector;

  /*!
  Stops the ros node and exits the application with an error_code
  */
  void fatal_error(const std::string& msg);

  /*!
  Is called when a dynamic parameter is changed
  */
  void reconfig_callback(scigl_render_ros::projectorConfig& config);

  /*!
  Is called periodically by the timer to trigger the rendering
  */
  void render_timer_callback(const ros::TimerEvent& timer_event);

  /*!
  Update the projectors intrinsic and extrinsic calibration parameters.
  /param path the yml file
  /returns true on success, false otherwise
  */
  bool set_projector_calibration(const std::string& path);
};
}  // namespace scigl_render_ros
