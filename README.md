# Table of Contents
[[_TOC_]]

# About
ROS wrapper for the [scigl_render](https://gitlab.com/rwth-irt-public/flirt/scigl_render) scientific OpenGL rendering library.

# Requirements
An OpenGL driver supporting core profile 4.2 is required.
NVIDIA drivers seem to work well, we experienced some weird issues with the Intel Mesa drivers.
You can check your version via:
```bash
glxinfo | grep 'version'
```

# Setup
We provide some Blender 3D-models with scigl_render which can be used to test this library.
To clone these models, [Git LFS](https://git-lfs.github.com/) has to be installed first.
Moreover we use the [wstool](http://wiki.ros.org/wstool) to download sources which are only available in repositories.

```bash
# install Git LFS
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash
# install wstool
sudo apt install python-wstool
```

Now we can setup the a catkin workspace via the wstool.
You might want to change the URL whether you use SSH or HTTPS to clone repositories.
```bash
# init workspace
mkdir -p my_catkin_ws && cd my_catkin_ws
wstool init src 
# add scigl_render_ros
wstool set -y -t src --git scigl_render_ros https://gitlab.com/rwth-irt-public/flirt/scigl_render_ros.git
wstool update -t src
# add and download dependencies of scigl_render
wstool merge -t src src/scigl_render_ros/.rosinstall
wstool update -t src
```

After setting up the workspace, the system dependencies can be installed via rosdep:
```bash
rosdep install --from-paths src --ignore-src -r -y
```

# Build the Catkin Workspace
As `catkin_make` does not support mixed CMake and catkin workspaces, you must use either `catkin_make_isolated` oder `catkin build` to compile the workspace.
We recommend the latter for faster compile times on multi-core system.
It is part of the [catkin_tools](https://catkin-tools.readthedocs.io/en/latest/index.html).

# Usage
This project contains four nodes:
- *ar_render_node*: Renders a 3D model on top of an calibrated camera image topic.
- *dummy_motion_node*: Simulates simple motion and publishes via tf. 
- *depth_render_node*: Publishes rendered depth images of a model
- *projector_node*: Renders a 3D model on a black background.

The pose of the camera, object and lights is determined by using tf2_ros.
Thus, it is required to set the correct frame ids for the object, camera and light.
We provide an example launch file which requires a calibrated [usb_cam](http://wiki.ros.org/usb_cam): [launch/demo.launch](launch/demo.launch).

# ar_render node
Renders a 3D model on top of an image topic.
The camera has to be [calibrated](http://wiki.ros.org/camera_calibration) and publish the camera description.

For the calibration you could use [this pattern](https://nerian.de/nerian-content/downloads/calibration-patterns/pattern-a4.pdf) and run the following command.
Please replace the camera topic an name with your camera.

```bash
rosrun camera_calibration cameracalibrator.py --size 4x11 --square 0.02 image:=/usb_cam/image_raw camera:=/usb_cam --pattern=acircles
```

An example launch file can be found in [launch/ar_render.launch](launch/ar_render.launch).

![Output of ar_render_node](images/ar_render.png)

*Output of the ar_render_node on top of a calibrated webcam.*

## Topics
Subscribed:
- `/camera/color/image_raw` : camera image to render on top
- `/tf` : transformation of the scene objects

Published:
- `/ar_camera/color/image_raw` : image with AR rendering

## Parameters
Scene frames:
- `light_frame_id` : tf frame id of the scene light
- `object_frame_id` : tf frame id of the object to render
- `world_frame_id` : tf frame id of the world (e.g. "world" or "map")

Rendering:
- `mesh_path` : path of the 3D model to render (Assimp supported formats like COLLADA)


# depth_render node
Renders as a depth image and publishes the depth image.

![Output of depth_render_node](images/depth_render.png)

*Output of the depth_render_node.*

## Topics
Subscribed:
- `/tf` : transformation of the scene objects

Published:
- `/render_camera/depth/image` : image with AR rendering

## Parameters
Scene Frames:
- `camera_frame_id` : tf frame id of the virtual camera
- `object_frame_id` : tf frame id of the object to render
- `world_frame_id` : tf frame id of the world (e.g. "world" or "map")

Rendering:
- `frame_rate` : How often the depth images are rendered and published
- `mesh_path` : path of the 3D model to render (Assimp supported formats like COLLADA)

Virtual Camera Parameters
- `width` : x-size of the images
- `height` : y-size of the images
- `near` : near distance of the camera frustum
- `far` : far distance of the camera frustum
- `c_x`, `c_y` : horizontal and vertical offset
- `f_x`, `f_y` : horizontal and vertical focal length
- `s` : skew factor

# dummy_motion_node
Simple simulation of a linear motion.

## Topics
Publishes:
- `tf` : transformation of the object

## Parameters
Scene Frames:
- `parent_frame_id` : publish the object transformation in this base frame
- `object_frame_id` : tf frame id of the simulated object
- 
Simulation:
- `frame_rate` : rate of the simulation
- `min_z` : move from this z position
- `max_z` : return at this z position
- `v_x`, `v_y`, `v_z` : translational speed components
- `rv_x`, `rv_y`, `rv_z` : rotational speed components
- `p_x`, `p_y`, `p_z`, `r_x`, `r_y`, `r_z` : inital position

# projector_node
Renders a 3D model on a black background.
It requires a [calibrated projector](https://gitlab.com/rwth-irt-public/flirt/utils/3d-projector-calibration) which uses the same parameters as an [OpenCV camera](https://docs.opencv.org/master/d9/d0c/group__calib3d.html).

![Screenshot of projector_node](images/projector.png)

*Screenshot of the projector_node.*

## Topics

Subscribed:
- `/tf` : transformation of the scene objects

Published:
- `/tf` : extrinsic parameters of the projector

## Parameters
Scene Frames:
- `camera_frame_id` : tf frame id of the camera, required to publish the extrinsics relative to it
- `light_frame_id` : tf frame id of the scene light
- `object_frame_id` : tf frame id of the object to render
- `projector_frame_id` : tf frame id of the projector from which to render
- `world_frame_id` : tf frame id of the world (e.g. "world" or "map")

Rendering:
- `frame_rate` : How often the images are rendered
- `mesh_path` : path of the 3D model to render (Assimp supported formats like COLLADA)

## Dynamic Reconfiguration
The ROS dynamic reconfiguration can be used to enter and exit the fullscreen mode.
Drag the window onto the desired screen and then enable the fullscreen mode.

# Dependencies
Installable via rosinstall:
- [gl3w_ros](https://github.com/Tuebel/gl3w_ros.git) - ROS wrapper for Loading OpenGL core profile
- [scigl_render](https://gitlab.com/rwth-irt-public/flirt/scigl_render) - scientific rendering library

All other dependencies are in the *package.xml* and will be pulled by rosdep.

# Continous Integration
A gitlab CI pipeline is implemented, which is based on [ros_gitlab_ci](https://gitlab.com/VictorLamoine/ros_gitlab_ci). 
The CI is based on the `ros:melodic-ros-core` docker image and will install the dependencies via rosdep. The gitlab-runner must use a **docker** executor.

# Special Thanks
If you want to learn more about the OpenGL implementation, please visit Joey de Vries website https://learnopengl.com/ and his twitter page https://twitter.com/JoeyDeVriez.
Thanks to his awesome resources I managed to write this library in a bit more than a week.

# Citing
If you use parts of this library in your scientific publication, please consider citing:
```
@article { 3Dcamerabasedmarkerlessnavigationsystemforroboticosteotomies,
      author = "Tim Übelhör and Jonas Gesenhues and Nassim Ayoub and Ali Modabber and Dirk Abel",
      title = "3D camera-based markerless navigation system for robotic osteotomies",
      journal = "at - Automatisierungstechnik",
      year = "01 Oct. 2020",
      publisher = "De Gruyter",
      address = "Berlin, Boston",
      volume = "68",
      number = "10",
      doi = "https://doi.org/10.1515/auto-2020-0032",
      pages=      "863 - 879",
      url = "https://www.degruyter.com/view/journals/auto/68/10/article-p863.xml"
}
```

# Funding
Funded by the Excellence Initiative of the German federal and state governments.
